cd ../../
call mvn clean install
if ERRORLEVEL 1 goto end

cd deployment/maven
call ant -f build_xliff-lib.xml
if ERRORLEVEL 1 goto end

cd ../../applications/integration-tests
call mvn -q integration-test

:end
pause