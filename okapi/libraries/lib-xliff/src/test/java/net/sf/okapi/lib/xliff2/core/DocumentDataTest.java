package net.sf.okapi.lib.xliff2.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import net.sf.okapi.lib.xliff2.core.StartXliffData;

import org.junit.Test;

public class DocumentDataTest {

	@Test
	public void testDefaults () {
		StartXliffData dd = new StartXliffData(null);
		assertEquals("2.0", dd.getVersion());
		assertEquals(null, dd.getSourceLanguage());
		assertEquals(null, dd.getTargetLanguage());
		assertTrue(dd.getExtAttributes().isEmpty());
	}
	
}
